<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Iman\Streamer\VideoStreamer;
use File;

class VideoController extends Controller
{
    //

    public function serve(Request $request, $account, $original){

        $invalidateCache = false;

        $extension = collect(explode('.',$original))->last();

        $encryptedOriginal = md5($original);

        $saveFolder = storage_path() . '/app/public/videos/' . $account . '/';

        if (!File::isDirectory($saveFolder)) {
            File::makeDirectory($saveFolder, 0755, true, true);
        }

        $savePath = $saveFolder . $encryptedOriginal.'.'.$extension;

        if (file_exists($savePath) && !$invalidateCache) {
            VideoStreamer::streamFile($savePath);
        }else{
            $video = file_get_contents($original);
            file_put_contents($savePath, $video);
            return 'Download in progress, refresh page in a few seconds...';
        }

        VideoStreamer::streamFile($savePath);

    }

}
